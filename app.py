#!/usr/bin/env python3

import os, sys, argparse, logging, json, base64
import requests
from functools import lru_cache
from bottle import route, request, response, redirect, default_app, view, template, static_file

def set_content_type(fn):
	def _return_json(*args, **kwargs):
		response.headers['Content-Type'] = 'application/json'
		if request.method != 'OPTIONS':
			return fn(*args, **kwargs)
	return _return_json

def enable_cors(fn):
	def _enable_cors(*args, **kwargs):
		response.headers['Access-Control-Allow-Origin'] = '*'
		response.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, OPTIONS'
		response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'

		if request.method != 'OPTIONS':
			return fn(*args, **kwargs)
	return _enable_cors

@route('/get/<ip>', method=('OPTIONS', 'GET'))
@enable_cors
@set_content_type
@lru_cache(maxsize=256)
def fetch(ip):
	try:
		data = requests.get('{}/get/{}'.format(args.base, ip), timeout=5).json()
		return data
	except:
		response.status_code = 400
		return json.dumps({
			"success": False,
			"message": "Unable to connect to backend server"
		})

@route('/ping')
def ping():
	response.content_type = "text/plain"
	return "pong"

@route('/')
def index():
	if request.query.q != "":
		return fetch(request.query.q)
	return "abuse.cloudproxy: a pretty frontend to abuse.cloud"

if __name__ == '__main__':

	parser = argparse.ArgumentParser()

	# Server settings
	parser.add_argument("-i", "--host", default=os.getenv('IP', '127.0.0.1'), help="server ip")
	parser.add_argument("-p", "--port", default=os.getenv('PORT', 5000), help="server port")

	# Application settings
	parser.add_argument("-b", "--base", default=os.getenv('APP_BASEURL', 'https://cfwho.com'), help="define hostname to proxy")
	parser.add_argument("-t", "--timeout", default=os.getenv('APP_TIMEOUT', 5), help="default timeout for requests to base URL")
	
	# Verbose mode
	parser.add_argument("--verbose", "-v", help="increase output verbosity", action="store_true")
	args = parser.parse_args()

	if args.verbose:
		logging.basicConfig(level=logging.DEBUG)
	else:
		logging.basicConfig(level=logging.INFO)
	log = logging.getLogger(__name__)

	try:
		app = default_app()
		app.run(host=args.host, port=args.port, server='tornado')
	except:
		log.error("Unable to start server on {}:{}".format(args.host, args.port))